const mongoose = require('mongoose');

const userSchema = new mongoose.Schema({
	firstName: {
		type: String, 
		required: [true, "First name is required."]
	},
	lastName: {
		type: String,
		required: [true, "Last name is required."]
	},
	email: {
		type: String,
		required: [true, "Email is required."]
	},
	password: {
		type: String,
	},

	categoriesMade: [
		{
			categoryName: {
				type: String,
				required: true
			},
			categoryType: {
				type: String,
				required: true
			}
		}

	],

	records: [
		{	
			recordName: {
				type: String,
				required: [true, "record name"]

			},

			recordType: {
				type: String,
				required: [true, "expense or income?"]
			},

			category: {
				type: String,
				required: [true, "which category?"]
			},
			dateMade: {
				type: Date,
				default: new Date()
			},
			amount: {
				type: Number,
				required: [true, "amount."]
			}

		}

	]
})

module.exports = mongoose.model('user', userSchema)