const User = require("../models/User")
const UserController = require("../controllers/user")
const express = require('express')
const router = express.Router()
const auth = require("../auth")


router.get("/", (req, res) => {

	UserController.getAllUsers().then(resultFromGetAllUsers => res.send(resultFromGetAllUsers))

})

router.get('/details', auth.verify, (req, res) => {
	
	const user = auth.decode(req.headers.authorization)

	UserController.get({userId: user.id}).then(user => res.send(user))
	
})

//this will get all the categories made by user
router.get('/categories', auth.verify, (req, res) => {

	let user = auth.decode(req.headers.authorization)

	let params = user.id

	UserController.getCategories(params).then(resultFromGetAllCategories => res.send(resultFromGetAllCategories))

})

//this will get all the records
router.get('/records', auth.verify, (req, res) => {

	let user = auth.decode(req.headers.authorization)

	let params = user.id

	UserController.getRecords(params).then(resultFromGetRecords => res.send(resultFromGetRecords))

})

router.post('/', (req, res) => {

	UserController.register(req.body).then(resultFromRegister => res.send(resultFromRegister))
	
})

router.post('/email-exists', (req,res)=>{
	UserController.emailExists(req.body).then(resultFromEmailExists => res.send(resultFromEmailExists))

})

router.post('/login', (req, res)=>{

	UserController.login(req.body).then(resultFromLogin => res.send(resultFromLogin))

})

router.post('/add-category', auth.verify, (req,res) => {

	//console.log(auth.decode(req.headers.authorization))

	//console.log(req.body)

	let userData = auth.decode(req.headers.authorization)

	//console.log(userData.id)


	let params = {

		userId: userData.id,
		categoryName: req.body.categoryName,
		categoryType: req.body.categoryType

	}

	UserController.addCategory(params).then(resultFromAddCategory => res.send(resultFromAddCategory))

})

//route for adding records
router.post('/add-record', auth.verify, (req,res) => {

	//console.log(auth.decode(req.headers.authorization))

	//console.log(req.body)

	let userData = auth.decode(req.headers.authorization)

	//console.log(userData.id)


	//only enter the Name, Type, Category and Amount
	let params = {

		userId: userData.id,
		recordName: req.body.recordName,
		recordType: req.body.recordType,
		category: req.body.category,
		amount: req.body.amount

	}

	UserController.addRecord(params).then(resultFromAddRecord => res.send(resultFromAddRecord))

})


module.exports = router
