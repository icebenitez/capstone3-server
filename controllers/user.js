const User = require("../models/User")

const bcrypt = require("bcrypt")

const auth = require("../auth")



module.exports.emailExists = (parameterFromRoute) => {

	return User.find({ email: parameterFromRoute.email }).then(resultFromFind => {

		return resultFromFind.length > 0 ? true : false

	})
}


module.exports.register = (params) => {

	console.log(params)

	let newUser = new User({

		firstName: params.firstName,
		lastName: params.lastName,
		email: params.email,		
		password: bcrypt.hashSync(params.password, 10)

	})

	return newUser.save().then((user, err)=>{

		return (err) ? false : true

	})

	console.log(user)

}


module.exports.login = (params) => {

	return User.findOne({email: params.email}).then(resultFromFindOne => {

		if(resultFromFindOne == null) {

			return false

		}

		const isPasswordMatched = bcrypt.compareSync(params.password, resultFromFindOne.password)

		console.log(isPasswordMatched)

		if(isPasswordMatched){

			return { accessToken: auth.createAccessToken(resultFromFindOne)}

		} else {

			return false
		}



	})

}


module.exports.get = (params) => {

	console.log(params)

	return User.findById(params.userId).then(resultFromFindById => { 

		resultFromFindById.password = undefined

		return resultFromFindById
	
	})

}

//this will find the user from db, and then return all items in categoriesMade array.
module.exports.getCategories = (params) => {

	//console.log(params)

	return User.findById(params).then(resultFromFindById => { 

		resultFromFindById = resultFromFindById.categoriesMade

		return resultFromFindById
	
	})

}

module.exports.getRecords = (params) => {

	return User.findById(params).then(resultFromFindRecords => { 

		resultFromFindRecords = resultFromFindRecords.records

		return resultFromFindRecords
	
	})
}

module.exports.addCategory = (params) => {

	//console.log(true)

	return User.findById(params.userId).then(resultFromFindByIdUser => {

		resultFromFindByIdUser.categoriesMade.push({categoryName: params.categoryName, categoryType: params.categoryType})

		return resultFromFindByIdUser.save().then((resultFromAddCategory, err) => {

				return (err) ? false : true		
			
		})
	})	

}


module.exports.addRecord = (params) => {

	console.log(params)

	return User.findById(params.userId).then(resultFromFindUserById => {

		resultFromFindUserById.records.push({
			recordName: params.recordName, 
			recordType: params.recordType,
			category: params.category, 
			amount: params.amount
		})

		return resultFromFindUserById.save().then((resultFromAddRecord, err) => {

				return (err) ? false : true		
			
		})
	})	

}